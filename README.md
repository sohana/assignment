<p align="center">Credential</p>
<p> => Admin :</p>
<p>Email : sohana@gmail.com</p>
<p>Password : 12345678</p>

<p> => Student :</p>
<p>Email : barna@gmail.com</p>
<p>Password : 12345678</p>

<p align="center">Route</p>
<p> => Login : /login</p>
<p>=> Student Registration : /register</p>

<p align="center">Project Setup</p>
<p>
=> Clone project
</p>
<p>
=> Go to the folder application using cd command on your cmd or terminal
</p>

<p>
=> Run composer install on your cmd or terminal
</p>
<p>
=> Run npm install on your cmd or terminal
</p>
<p>
=> Copy .env.example file to .env on the root folder. You can type copy .env.example .env if using command prompt Windows.
Open your .env file and change the database name (DB_DATABASE) to whatever you have, username (DB_USERNAME) and password (DB_PASSWORD) field correspond to your configuration.
By default, the username is root and you can leave the password field empty. (This is for Xampp)
By default, the username is root and password is also root.
</p>
<p>
=> Run php artisan key:generate
</p>
<p>
=> Run php artisan migrate:fresh --seed
</p>
<p>
=> Run php artisan serve
</p>
<p>
=> npm run watch
</p>



