<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $rules = [
            'email' => 'required|string',
            'password' => 'required|max:15|min:6',
        ];
        $validator = Validator::make($credentials, $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message'=> implode(", " , $validator->messages()->all())]);
        }

        $user = User::select('id', 'name', 'email')->where('email', $request->email)->first();
        if (empty($user)) {
            return response()->json(['status' => false, 'message' => 'Unauthorized']);
        }

        if (Hash::check($request->password, $user->password)) {
            return response()->json(['status' => false, 'message' => 'Password does not match!']);
        }

        $token = $user->createToken('user')->plainTextToken;

        return response()->json([
            'status'=> true,
            'message'=> 'Login Successful.',
            'token' => $token,
            'data' => $user,
        ], 200);
    }

    public function logout(Request $request)
    {
        $user = $request->user();
        $user->tokens()->delete();

        return response()->json([
            'success'=> true,
            'message' => 'Successfully logged out'
        ], 200);
    }

    public function register(Request $request)
    {
        $credentials = $request->only('name', 'email', 'password');

        $rules = [
            'name' => 'required|string',
            'email' => 'required|string|unique:users,email',
            'password' => 'required|max:15|min:8',
        ];

        $validator = Validator::make($credentials, $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message'=> implode(", " , $validator->messages()->all())]);
        }

        $storeData = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'type' => 'Student',
        ];

        $user = User::create($storeData);
        $user->assignRole('Student');

        if (!empty($user)) {
            return response()->json(['status' => true, 'message' => 'Successfully registered!', 'data' => $user]);
        } else {
            return response()->json(['status' => false, 'message' => 'Error!']);
        }


    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
