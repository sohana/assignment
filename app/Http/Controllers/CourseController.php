<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\CourseUnit;
use App\Models\EnrollCourse;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    public function index()
    {
        $courses = Course::orderBy('created_at', 'asc')->get();

        return response()->json([
            'status' => true,
            'data' => $courses,
        ], 200);
    }

    public function store(Request $request)
    {
        $requestData = $request->all();
        $validator = Validator::make($requestData, [
            'code' => 'required|max:255|unique:courses,code',
            'title' => 'required|max:255',
            'description' => 'required|max:65535',
            'thumbnails' => 'required|image|mimes:jpeg,png,jpg|max:500',

        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => implode(", ", $validator->messages()->all())]);
        }
        $storeData = [
            'code' => $request->code,
            'title' => $request->title,
            'description' => $request->description,
        ];

        if ($request->hasFile('thumbnails')) {
            $image = (new MediaController())->imageUpload($request->file('thumbnails'), 'course');
            $storeData['thumbnails'] = $image['name'];
        }

        $course = Course::create($storeData);

        if (!empty($course)) {
            return response()->json(['status' => true, 'message' => 'Successfully Saved!', 'data' => $course]);
        } else {
            return response()->json(['status' => false, 'message' => 'Error!']);
        }
    }

    public function show($id)
    {
        $course = Course::find($id);
        return response()->json([
            'status' => true,
            'data' => $course,
        ], 200);
    }

    public function edit($id)
    {
        $course = Course::find($id);
        return response()->json([
            'status' => true,
            'data' => $course,
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        $validator = Validator::make($requestData, [
            'code' => 'required|max:255|unique:courses,code,' . $id . ',id',
            'title' => 'required|max:255',
            'description' => 'required|max:65535',
            'thumbnails' => 'required|image|mimes:jpeg,png,jpg|max:500',

        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => implode(", ", $validator->messages()->all())]);
        }
        $course = Course::find($id);
        $updateData = [
            'code' => $request->code,
            'title' => $request->title,
            'description' => $request->description,
            'thumbnails' => $request->thumbnails,
        ];

        if ($request->hasFile('thumbnails')) {
            (new MediaController())->delete( 'course', $course->thumbnails);

            $image = (new MediaController())->imageUpload($request->file('thumbnails'), 'course');
            $updateData['thumbnails'] = $image['name'];
        }

        $course->update($updateData);
        if (!empty($course)) {
            return response()->json(['status' => true, 'message' => 'Successfully updated!', 'data' => $course]);
        } else {
            return response()->json(['status' => false, 'message' => 'Error!']);
        }
    }

    public function destroy($id)
    {
        $course = Course::find($id);
        $course->delete();
        return response()->json([
            'status' => true,
            'message' => 'Successfully deleted!',
        ], 200);
    }

    public function enroll(Request $request)
    {
        $requestData = $request->all();
        $validator = Validator::make($requestData, [
            'course_id' => 'required|integer',

        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => implode(", ", $validator->messages()->all())]);
        }
        $userId =Auth::User()->id;
        $storeData = [
            'course_id' => $request->course_id,
            'user_id' => $userId,
        ];
        $enroll = EnrollCourse::create($storeData);

        if (!empty($enroll)) {
            return response()->json(['status' => true, 'message' => 'Successfully Saved!', 'data' => $enroll]);
        } else {
            return response()->json(['status' => false, 'message' => 'Error!']);
        }
    }

    public function enrolledCourseList()
    {
        $enrolledCourse = Course::with('units')->whereHas('enroll', function($q){
            $q->where('user_id', Auth::User()->id);
        })->get();
        return response()->json([
            'status' => true,
            'data' => $enrolledCourse,
        ], 200);
    }
}
