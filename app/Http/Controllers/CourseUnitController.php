<?php

namespace App\Http\Controllers;

use App\Models\CourseUnit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CourseUnitController extends Controller
{

    public function index()
    {
        $units = CourseUnit::with('course')->orderBy('created_at', 'asc')->get();

        return response()->json([
            'status' => true,
            'data' => $units,
        ], 200);
    }

    public function store(Request $request)
    {
        $requestData = $request->all();
        $validator = Validator::make($requestData, [
            'course_id' => 'required|integer',
            'code' => 'required|max:255|unique:course_units,code',
            'title' => 'required|max:255',
            'description' => 'required|max:65535',
            'unit_file' => 'required|mimes:pdf|max:500'

        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => implode(", ", $validator->messages()->all())]);
        }
        $storeData = [
            'course_id' => $request->course_id,
            'code' => $request->code,
            'title' => $request->title,
            'description' => $request->description,
        ];

        $check = CourseUnit::count();
        if($check>0) {
            $storeData['status'] = 'Incomplete';
        } else {
            $storeData['status'] = 'Complete';
        }

        if ($request->hasFile('unit_file')) {
            $image = (new MediaController())->imageUpload($request->file('unit_file'), 'unit');
            $storeData['unit_file'] = $image['name'];
        }

        $unit = CourseUnit::create($storeData);

        if (!empty($unit)) {
            return response()->json(['status' => true, 'message' => 'Successfully Saved!', 'data' => $unit]);
        } else {
            return response()->json(['status' => false, 'message' => 'Error!']);
        }
    }


    public function show($id)
    {
        $unit = CourseUnit::with('course')->find($id);
        return response()->json([
            'status' => true,
            'data' => $unit,
        ], 200);
    }


    public function edit($id)
    {
        $unit = CourseUnit::with('course')->find($id);
        return response()->json([
            'status' => true,
            'data' => $unit,
        ], 200);
    }


    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        $validator = Validator::make($requestData, [
            'course_id' => 'required|integer',
            'code' => 'required|max:255|unique:course_units,code,' . $id . ',id',
            'title' => 'required|max:255',
            'description' => 'required|max:65535',
            'unit_file' => 'required|mimes:pdf|max:500'

        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => implode(", ", $validator->messages()->all())]);
        }
        $unit = CourseUnit::find($id);
        $updateData = [
            'course_id' => $request->course_id,
            'code' => $request->code,
            'title' => $request->title,
            'description' => $request->description,
        ];

        if ($request->hasFile('unit_file')) {
            (new MediaController())->delete( 'unit', $unit->unit_file);

            $image = (new MediaController())->imageUpload($request->file('unit_file'), 'unit');
            $updateData['unit_file'] = $image['name'];
        }

        $unit->update($updateData);
        if (!empty($unit)) {
            return response()->json(['status' => true, 'message' => 'Successfully updated!', 'data' => $unit]);
        } else {
            return response()->json(['status' => false, 'message' => 'Error!']);
        }
    }


    public function destroy($id)
    {
        $unit = CourseUnit::find($id);
        $unit->delete();
        return response()->json([
            'status' => true,
            'message' => 'Successfully deleted!',
        ], 200);
    }

    public function statusUpdate(Request $request)
    {
        $credentials = $request->all();
        $validator = Validator::make($credentials, [
            'unit_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => implode(", ", $validator->messages()->all())]);
        }
        $sql = CourseUnit::select('id')->where('status', 'Incomplete')->orderBy('id', 'asc')->first();
        $unit = $sql->find($sql->id);
        if (empty($unit)) {
            return response()->json(['status' => false, 'message' => 'Data not found!']);
        }

        $unit->update(['status' => 'Complete']);

        return response()->json(['status' => true, 'message' => 'Status updated Successfully!', 'data' => $unit]);
    }
}
