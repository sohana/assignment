<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function user() {
        $user = User::where('id', Auth::user()->id)->first();
        return response()->json([
            'status' => true,
            'data' => $user,
        ], 200);
    }
}
