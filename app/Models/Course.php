<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Course extends Model
{
    use HasFactory;

    protected $fillable = [
        'code', 'title', 'description', 'thumbnails'
    ];

    public function units()
    {
        return $this->hasMany(CourseUnit::class, 'course_id', 'id');
    }
    public function enroll()
    {
        return $this->hasOne(EnrollCourse::class, 'course_id', 'id');
    }
}
