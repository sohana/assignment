<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'create-course',
            'view-course',
            'download-course',
            'create-unit',
            'view-unit',
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission, 'guard_name' => 'web']);
        }

        Role::create(['name' => 'Admin', 'guard_name' => 'web'])->givePermissionTo(['create-course', 'create-unit']);
        Role::create(['name' => 'Student', 'guard_name' => 'web'])->givePermissionTo(['view-course', 'download-course', 'view-unit']);
    }
}
