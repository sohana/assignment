<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Sohana',
            'email' => 'sohana@gmail.com',
            'password' => bcrypt('12345678'),
            'type' => 'Admin',
        ]);

        $user->assignRole('Admin');

        $user = User::create([
            'name' => 'Barna',
            'email' => 'barna@gmail.com',
            'password' => bcrypt('12345678'),
            'type' => 'Student',
        ]);

        $user->assignRole('Student');
    }
}
