/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

import Vue from 'vue'
import VueAxios from 'vue-axios';
import VueRouter from 'vue-router'
import axios from 'axios';
const token = localStorage.getItem('_auth_token');
if (token) {
    axios.defaults.headers.common.Authorization = `Bearer ${token}`;
}

Vue.use(VueRouter);
Vue.use(VueAxios, axios);

Vue.component('pagination', require('laravel-vue-pagination'));
Vue.prototype.$userId = document.querySelector(["meta[name='user-id']","meta[name='userId']"]).getAttribute('content');

import LoginComponent from "./components/pages/LoginComponent";
import RegistrationComponent from "./components/pages/RegistrationComponent";
import DashboardComponent from "./components/pages/DashboardComponent";
import CourseListComponent from "./components/pages/course/CourseListComponent";
import CourseAddComponent from "./components/pages/course/CourseAddComponent";
import CourseEditComponent from "./components/pages/course/CourseEditComponent";
import CourseDetailsComponent from "./components/pages/course/CourseDetailsComponent";
import UnitListComponent from "./components/pages/unit/UnitListComponent";
import UnitAddComponent from "./components/pages/unit/UnitAddComponent";
import UnitEditComponent from "./components/pages/unit/UnitEditComponent";
import UnitDetailsComponent from "./components/pages/unit/UnitDetailsComponent";
import OfferCourseComponent from "./components/pages/course/OfferCourseComponent";
import EnrolledCourseComponent from "./components/pages/course/EnrolledCourseComponent";

const auth = (to, from, next) => {
    if (!localStorage.getItem('_auth_token')) {
        return router.push({ path: '/' });
    }
    return next();
}

const routes = [
    { path: '/', component: LoginComponent },
    { name: 'login', path: '/login', component: LoginComponent },
    { path: '/register', component: RegistrationComponent },
    { path: '/dashboard', component: DashboardComponent, beforeEnter: auth },
    { name: 'course', path: '/course', component: CourseListComponent, beforeEnter: auth },
    { path: '/offer', component: OfferCourseComponent, beforeEnter: auth },
    { path: '/enrolled', component: EnrolledCourseComponent, beforeEnter: auth },
    { name: 'course.create', path: '/course/create', component: CourseAddComponent, beforeEnter: auth },
    { name: 'course.edit', path: '/course/edit/:id', component: CourseEditComponent, beforeEnter: auth },
    { name: 'course.show', path: '/course/show/:id', component: CourseDetailsComponent, beforeEnter: auth },
    { name: 'unit', path: '/unit', component: UnitListComponent, beforeEnter: auth },
    { path: '/unit/create', component: UnitAddComponent, beforeEnter: auth },
    { name: 'unit.edit', path: '/unit/edit/:id', component: UnitEditComponent, beforeEnter: auth },
    { name: 'unit.show', path: '/unit/show/:id', component: UnitDetailsComponent, beforeEnter: auth },
]

const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
})

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('login-component', require('./components/pages/LoginComponent.vue').default);
Vue.component('header-component', require('./components/layouts/HeaderComponent.vue').default);
Vue.component('sidebar-component', require('./components/layouts/SidebarComponent.vue').default);
Vue.component('footer-component', require('./components/layouts/FooterComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    router,
    el: '#app',
});
