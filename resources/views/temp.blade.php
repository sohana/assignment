@extends('layouts.vue')
@section('title')
    Home
@endsection
@section('content')
    <section class="content">
        <router-view></router-view>
    </section>

@endsection
