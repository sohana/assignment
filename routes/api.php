<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\CourseUnitController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', [LoginController::class, 'register']);
Route::post('/login', [LoginController::class, 'login']);
//Route::post('/register', [RegisterController::class, 'register']);
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/user', [UserController::class, 'user']);
    Route::post('/logout', [LoginController::class, 'logout']);
    Route::resource('courses', CourseController::class);
    Route::post('/enroll/courses', [CourseController::class, 'enroll']);
    Route::get('/enrolled/courses', [CourseController::class, 'enrolledCourseList']);
    Route::resource('course-unit', CourseUnitController::class);
    Route::post('/status/update', [CourseUnitController::class, 'statusUpdate']);

});
